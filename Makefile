# nnn-preview - simple nnn previewer script

include config.mk

SRC = nnn-preview
SCR = scripts

default: install

clean:
	rm -f nnn-preview-${VERSION}.tar.xz

dist: clean
	mkdir -p nnn-preview-${VERSION}
	cp -R ${SRC} ${SCR} \
		Makefile config.mk \
		LICENSE README \
		nnn-preview-${VERSION}
	tar -cf nnn-preview-${VERSION}.tar nnn-preview-${VERSION}
	xz -9 -T0 nnn-preview-${VERSION}.tar
	rm -rf nnn-preview-${VERSION}

install:
	mkdir -p ${PREFIX}/bin
	mkdir -p ${NNNCONF}
	cp -f nnn-preview ${PREFIX}/bin
	cp -rf ${SCR} ${NNNCONF}
	chmod 755 ${PREFIX}/bin/nnn-preview

uninstall:
	rm -r ${PREFIX}/bin/nnn-preview

.PHONY: clean dist install uninstall
